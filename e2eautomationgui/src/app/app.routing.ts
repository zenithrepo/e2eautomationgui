import { Routes } from '@angular/router';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { TableComponent } from './testsuite/table.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { TestcasemanageComponent } from "./testsuite/testcasemanage/testcasemanage.component";
import { SuitedetailsComponent } from './suite/suitedetails/suitedetails.component';
import { SuiteComponent } from './suite/suite.component';
import { HistoryComponent } from './history/history.component';
import { SuitemanageComponent } from './suite/suitemanage/suitemanage.component';
import { SettingsComponent } from './settings/settings.component';

export const AppRoutes: Routes = [
    {
        path: '',
        redirectTo: 'suite',
        pathMatch: 'full',
    },
    {
        path: 'dashboard',
        component: DashboardComponent
    },
    {
        path: 'case',
        component: TableComponent
    },
     {
        path: 'suite',
        component: SuiteComponent
    },{
        path: 'history',
        component: HistoryComponent
    },
     {
        path: 'suitedetails/:id/:id1/:id2',
        component: SuitedetailsComponent
    },
     {
        path: 'testcasemanage/:id',
        component: TestcasemanageComponent  // testcase manage
    },
     {
        path: 'suitemanage/:id',
        component: SuitemanageComponent
    },
      {
        path: 'settings',
        component: SettingsComponent
    },
   /* {
        path: 'typography',
        component: TypographyComponent
    },
    {
        path: 'icons',
        component: IconsComponent
    },
    {
        path: 'notifications',
        component: NotificationsComponent
    },
    {
        path: 'upgrade',
        component: UpgradeComponent
    }*/
]
