import { ServerResponse } from "../server.response";
import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { NgProgress } from 'ngx-progressbar';
import { TestCase } from './testcase';
import { throws } from 'assert';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}

@Component({
    selector: 'table-cmp',
    moduleId: module.id,
    templateUrl: 'table.component.html'
})


export class TableComponent implements OnInit{
    dtOptions: DataTables.Settings = {};
    public tableData1: TableData;
    private successAlert:boolean;
    private selectedSuiteId: number;
    private response: ServerResponse[];
    private TestCaseArray: TestCase[]; 
    private selectedTestCase:string;
 
    private baseUrl: string = 'http://localhost:9200/E2EAutomation/GUIController?param=';
    private header = new Headers({'Content-Type':'application/json'});
    private options = new RequestOptions({headers:this.header});

   dtTrigger: Subject<any> = new Subject();
  
   constructor( private http: Http,public ngProgress: NgProgress){}
  showCollapseDiv(TestCase)
  {
    this.selectedTestCase = TestCase.testcaseName;
    //this.TestCaseArray = Testsuite1.testCaseList;
    console.log(this.TestCaseArray);    
  }
  
  editTestCase(TestCase)
  {
    console.log('call log method');
    console.log(TestCase);
    TestCase.isEditable=!TestCase.isEditable
  }
   getTestCases()
  {
   console.log('getting suite data');
   return this.http.get(this.baseUrl + 'testcase', this.options).map((response: Response) => response.json()).catch(this.errorHandler);
     // (response:Response) => response.json()).catch(this.errorHandler)
  }
  /* connectToRunTS(testsuiteID){
    return this.http.get(this.baseUrl + 'run&sid=' + testsuiteID, this.options).map((response: Response) =>
       response.json()).catch(this.errorHandler);
  } 
  run(TestSuite)
  {
    console.log('Inside the Run method');
    console.log(TestSuite);
    this.connectToRunTS(TestSuite.suiteId).subscribe((resp) => {
    this.response = resp;
    console.log(this.response[0])      
    this.success(this.response[0].errorMessage);   
  }),(error) => {console.log("error");}
    console.log('Run completed');
  }
  */
  refreshTestCases()
  {
    console.log('refreshing testcases');
      this.getTestCases().subscribe((TestCases) => {
      this.TestCaseArray = TestCases;
        console.log(this.TestCaseArray)
      }),(error) => {
        console.log("error");
      }
  }
  
    goToQCUrl(url): void {
    window.location.href='http://:qualitycenter.tsl.telus.com/qcbin,QC_V10_PROJECTS,Wireless_Consolidated,[AnyUser];1:'+url;
  }
  
  removeAlert()
  {
     this.successAlert = false;
     this.response[0] = null;
  }
   success(message: string) {
     this.successAlert = true;
    }
  errorHandler(error:Response)
  {
    return Observable.throw(error || "Server connection error");
  }
   tsForm:boolean=false;
   isNewTS:boolean = false;
   displayTSTable:boolean = true;
   newTS:any={};
  
  cancelTS()
  {
    this.displayTSTable = !this.displayTSTable;
    this.tsForm=!this.tsForm;
    this.isNewTS=!this.tsForm;
  }
  showEditTSForm()
  {
    
  }  
  addStep()
  {
    
  }  
  saveTS()
  {
    
  }

  deletTestCaseHttp(tid:number) {
  console.log(" deletTestCaseHttp "+tid);  
        return this.http.get(this.baseUrl+'deletetest&tid='+tid).map((response: Response) => response.json()).catch(this.errorHandler);
    }

  removeTS(testcase:TestCase)
  {
    if (confirm('Are you sure to delete "'+testcase.testcaseName+'" Testcase ?') == true)
     {
      this.deletTestCaseHttp(testcase.testcaseId).subscribe(
       (data) => {
         //this.response = data;
         console.log("printing response data");
         console.log(data[0].errorMessage);
         console.log(data[0].status);
         alert(data[0].errorMessage);
          this.refreshTestCData();
       }
    ),(error) => {console.log("error");}
    console.log('delete completed');
     }
  }
   refreshTestCData()
{
this.getTestCases().subscribe((TestCases) => {
    this.TestCaseArray = TestCases;
    console.log(this.TestCaseArray);
  }),(error) => {
    console.log("error")
  }
}

    showAddTSForm(){
    this.tsForm=!this.tsForm;
    this.isNewTS=!this.tsForm;
    this.displayTSTable = !this.displayTSTable;
  }  
    ngOnInit(){
 this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25
    };
  this.getTestCases().subscribe((TestCases) => {
    this.TestCaseArray = TestCases;
    console.log(this.TestCaseArray);
      this.dtTrigger.next();
  }),(error) => {
    console.log("error")
  }
      
        
    }
}
