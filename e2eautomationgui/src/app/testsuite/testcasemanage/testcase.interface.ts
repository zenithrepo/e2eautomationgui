export interface TestCaseInterface {
  
  testcaseId: string;
  testcaseName: string;
  testcaseXml: string;
  testenv:string;
  tag:string;
  description:string;

}