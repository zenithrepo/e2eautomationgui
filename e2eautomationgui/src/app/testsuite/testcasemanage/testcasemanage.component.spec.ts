import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TestcasemanageComponent } from './testcasemanage.component';

describe('TestcasemanageComponent', () => {
  let component: TestcasemanageComponent;
  let fixture: ComponentFixture<TestcasemanageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TestcasemanageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestcasemanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
