import { Component, OnInit,NgModule , ViewChild} from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { TestCaseInterface } from './testcase.interface';
import {IOption} from 'ng-select';
import { ServerResponse } from "../../server.response";
import { NgProgress } from 'ngx-progressbar';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { TestCase } from '../testcase';

import { Http, Response, Headers, RequestOptions } from '@angular/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};
@Component({
  selector: 'app-testcasemanage',
  templateUrl: './testcasemanage.component.html',
  styleUrls: ['./testcasemanage.component.css']
})
export class TestcasemanageComponent implements OnInit {
  @ViewChild('editor') editor;
  public testForm: FormGroup;
  paramid: string;
  public submitted: boolean; // keep track on whether form is submitted
  public editTC: boolean;
  private baseUrl: string =  'http://localhost:9200/E2EAutomation/GUIController?param=';
  private header = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.header});
  private response: ServerResponse[];
   private tcase :TestCase;
  private testCaseInterface:TestCaseInterface;
  text:string = "";
  editoroptions:any = {maxLines: 1000, printMargin: false};

  
  constructor(private _fb: FormBuilder,
    private route: ActivatedRoute,
    private router: Router, 
    private http:HttpClient ,
    private normalhttp:Http,
    public ngProgress: NgProgress ) { }

   ngAfterViewInit() {
     this.editor.setMode("ace/mode/xml");
     this.editor.setTheme("eclipse");
      this.editor.getEditor().setOptions({
            enableBasicAutocompletion: true
        });
        this.editor.getEditor().commands.addCommand({
            name: "showOtherCompletions",
            bindKey: "Ctrl-.",
            exec: function (editor) {

            }
        })
   }

  ngOnInit() {
    this.paramid = this.route.snapshot.paramMap.get('id');
      this.testForm = this._fb.group({
        testcaseName: ['', [<any>Validators.required]],
        hpqcId: [''],
        description: [''],
        tag: [''],
        testcaseXml: ['']
      });
    
 /* (<FormGroup>this.testForm).setValue(testcaseName, 'Sample value name'); */
    
    // check the follwoing part
    this.editTC = false; 
    if( this.paramid != "0")
     {
       this.getTestCaseData(this.paramid);
    }
         
    
  }

    errorHandler(error:Response)
  {
    return Observable.throw(error || "Server connection error");
  }
  
    getTestCaseHTTP(tid:string) {
  
        return this.normalhttp.get(this.baseUrl+'gettest&tid='+tid).map((response: Response) => response.json()).catch(this.errorHandler);
    }

  getTestCaseData(tid:string)
  {   
        this.getTestCaseHTTP(tid).subscribe(
       (data) => {
         //this.response = data;
         console.log("printing response data");
        
         this.tcase =     data[0];
          console.log(this.tcase);    
          //this.testForm.setValue(this.tcase); 
         this.text =    this.tcase.testcaseXml;
          this.testForm.patchValue({description:  this.tcase.description}); 
          this.testForm.patchValue({tag:  this.tcase.tag});
          this.testForm.patchValue({testcaseName:  this.tcase.testcaseName});
          this.testForm.patchValue({hpqcId:  this.tcase.hpqcId});  
         this.editTC = true;               
       }
    ),(error) => {console.log("error");}
    console.log('get data completed');
     
  }
      
    onChange(code) {
        console.log("new test = ", code);
    }
  
  save(testcase:TestCaseInterface) {
   if (confirm('Are you sure to submit "'+testcase.testcaseName+'" Testcase ?') == true)
  {
      this.submitted = true;      
      testcase.testcaseXml = this.text;  
     if(this.editTC) 
        testcase.testcaseId  =this.paramid;
     
       this.createTestCase(testcase);
  }
}
  
  cancelForm()
  {
    console.log("cancel cal");
    this.router.navigate(['/case']);
  }
  
  createTestCase(testcase) {
    this.ngProgress.start();
    this.createTestCaseHttp(testcase).subscribe(
       (data) => {
         //this.response = data;
         console.log("printing response data");
         console.log(data[0].errorMessage);
         console.log(data[0].status);
         this.ngProgress.done();
         alert(data[0].errorMessage);
         this.router.navigate(['/case']);         
       }
    ),(error) => {console.log("error");}
    console.log('Run completed');
  }

   createTestCaseHttp(testcase) {
        let body = JSON.stringify(testcase);
        console.log("sending http post request for create testcase");
  if(this.editTC)
       {
        return this.http.post(this.baseUrl+'updatetest', body, httpOptions);
     }else
    {
    return this.http.post(this.baseUrl+'createtest', body, httpOptions); 
  }
     }
    

}
