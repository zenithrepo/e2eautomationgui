import { TestStep } from './teststep';
import { TestPrerequisities} from './testprerequisities'
import { TestAssert } from './testassert'

export class TestCase {
  testcaseId: number;
  testcaseName: string;
  testcaseXml: string;
  hpqcId: number;
  testenv: string;
  tag:string;
  description:string;
  creationDate:any;
  testPrerequisities: TestPrerequisities[];
  testAtepsArray: TestStep[];
  testAssert: TestAssert[];
  isEditable: boolean;
  }