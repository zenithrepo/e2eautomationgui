import { TestCase } from './testcase';
import { Timestamp } from 'rxjs';
export class Testsuite {
   suiteId:number;
   name:string;
   env:string;
   executionCounter:number;
   creationDate:string;
   testcases:TestCase[];  
}
