export class ServerResponse {
  status:string;
  errorMessage:string;
  duration:number;
}