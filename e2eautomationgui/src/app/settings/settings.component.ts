import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';

@Component({
  selector: 'app-responsive-extension',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {

  dtOptions: any = {};

  
  constructor( private http: Http, private router: Router ){}
  
  ngOnInit(): void {
    this.dtOptions = {
      columns: [{
        title: 'Connection_ID',
        data: 'id'
      },{
        title: 'DataBase',
        data: 'databaseType'
      }, {
        title: 'DataBase Host',
        data: 'host'
      }, {
        title: 'Port',
        data: 'port'
      }, {
        title: 'UserName',
        data: 'username'
      }, {
        title: 'Password',
        data: 'password'
      }, {
        title: 'Schema',
        data: 'schema',
        class: 'none'
      }],
      // Use this attribute to enable the responsive extension
      responsive: true
    };
    

   /*  this.getTestSuites().subscribe((TestSuites) => {
      this.TestSuiteArray = TestSuites;
      console.log(this.TestSuiteArray[0].executionCounter);        
        this.dtTrigger.next();        
        }),(error) => {
         console.log("error")
         }
    */
    
  }
}