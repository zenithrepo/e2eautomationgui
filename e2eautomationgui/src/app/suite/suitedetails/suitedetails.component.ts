import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { SuiteStatistics } from './suitestatistics';
import { TestRecordData } from './testrecorddata';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs';
import * as Chartist from 'chartist';

@Component({
  selector: 'app-suitedetails',
  templateUrl: './suitedetails.component.html',
  styleUrls: ['./suitedetails.component.css']
})
export class SuitedetailsComponent implements OnInit {
  testRecordData: Observable<TestRecordData>; 
  
  paramid: string;
  source: string;
  testInstance: number;
  private successAlert: boolean;
  private baseUrl: string ='http://localhost:9200/E2EAutomation/GUIController?param=';
  private header = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.header});
  private stepReportArray: TestRecordData[];
  private testReportArray: TestRecordData[];
  private suiteData: TestRecordData;
  private suitePercentage :SuiteStatistics;
  private suiteHistoryArray: TestRecordData[];
  private lableArray: string[]; 
  private seriesArray:number[];
  
  
 constructor(
    private route: ActivatedRoute,
    private router: Router,
    private http: Http 
   // private service: HeroService
  ) { }
  
  getSuiteDetails(id: number | string , instance: number | string)
  {
   console.log("connecting to server");
   return this.http.get(this.baseUrl+'suitedetails&sid='+id+'&instance='+instance, this.options).map((response:Response) => response.json()).catch(this.errorHandler)
  }
  errorHandler(error:Response)
  {
    return Observable.throw(error || "Server connection error");
  }
  
  
   getColor(status) { 
    switch (status) {
      case 'SUCCESS':
        return 'green';
      case 'SKIPPED':
        return 'blue';
      case 'FAILED':
        return 'red';
    }
  }
  
  
  isActiveOption(testInstance:string)
 {
  return false;
 }
  gethistory(suiteId:string ,testInstance:string)
  {
    this.successAlert = false;
    this.getSuiteDetails(suiteId,testInstance).subscribe((SuiteReport) => {
    console.log("response from server");
    this.suiteData =   SuiteReport[0].testRecord.suiteRecord;
    this.testReportArray =   SuiteReport[0].testRecord.testRecordList;
    this.stepReportArray =   SuiteReport[0].testRecord.stepRecordList;
    this.suiteHistoryArray =   SuiteReport[0].suiteRecordHistoryList;
    this.suitePercentage =  SuiteReport[0].statusStatisticsList[0];
    console.log(this.suitePercentage);
    this.lableArray= this.suitePercentage.labelsPerceantageArray;
    this.seriesArray =  this.suitePercentage.seriePerceantageArray;
    console.log(this.lableArray);
    console.log(this.seriesArray);
    new Chartist.Pie('#chartPreferences', {labels: this.lableArray, series: this.seriesArray});
  }),(error) => {
    console.log("error")
  }
  }
  removeAlert()
  {
     this.successAlert = false;
  }
    success(suiteID: string) {
    this.successAlert = true;

    }
    ngOnInit() {
     this.paramid = this.route.snapshot.paramMap.get('id');
     this.source = this.route.snapshot.paramMap.get('id1');
	  this.testInstance = +this.route.snapshot.paramMap.get('id2');
    if(this.source == "run")
    {
        this.success(this.paramid);
    }
    console.log("source ="+this.source);
    //this.getSuiteDetails(this.paramid); 
    console.log(this.paramid);
    this.getSuiteDetails(this.paramid,this.testInstance).subscribe((SuiteReport) => {
    console.log("response from server");
    this.suiteData =   SuiteReport[0].testRecord.suiteRecord;
    this.testReportArray =   SuiteReport[0].testRecord.testRecordList;
    this.stepReportArray =   SuiteReport[0].testRecord.stepRecordList;
    this.suiteHistoryArray =   SuiteReport[0].suiteRecordHistoryList;
    this.suitePercentage =  SuiteReport[0].statusStatisticsList[0];
    console.log(this.suitePercentage);
    this.lableArray= this.suitePercentage.labelsPerceantageArray;
    this.seriesArray =  this.suitePercentage.seriePerceantageArray;
    console.log(this.lableArray);
    console.log(this.seriesArray);
    new Chartist.Pie('#chartPreferences', {labels: this.lableArray, series: this.seriesArray});
  }),(error) => {
    console.log("error")
  }

   }

}
