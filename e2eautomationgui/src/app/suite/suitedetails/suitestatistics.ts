export class SuiteStatistics {
    labelsPerceantageArray: string[];
    seriePerceantageArray: number[];
    total:number;
    successTotal:number;
    successPercentage:number;
    failedTotal:number;
    failedPercentage:number;
    ignoredTotal:number;
    ignoredPercentage:number;
    pendingTotal:number;
    pendingPercentage:number;
    notActive:number;
}