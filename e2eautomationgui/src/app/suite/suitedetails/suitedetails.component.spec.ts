import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitedetailsComponent } from './suitedetails.component';

describe('SuitedetailsComponent', () => {
  let component: SuitedetailsComponent;
  let fixture: ComponentFixture<SuitedetailsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitedetailsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitedetailsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
