export class TestRecordData {
   testInstance:number;
   testcaseName:string;
   testcaseId:number;
   suiteId:number;
   suiteName:string;
   stepName:string;
   stepNumber:number;
   status:string;
   resultMessage:string;
   env:string;
   executedBy:string;
   startDate:string;
   completeDate:string;
   testLevel:number;
}