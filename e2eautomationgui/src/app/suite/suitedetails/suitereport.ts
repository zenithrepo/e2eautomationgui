import { SuiteStatistics } from './suitestatistics';
import { TestRecord } from './testrecord';
import { TestRecordData } from './testrecorddata';
export class SuiteReport {
  testRecord: TestRecord; 
  suiteRecordHistoryList : TestRecordData[];
  statusStatisticsList:SuiteStatistics[];
}