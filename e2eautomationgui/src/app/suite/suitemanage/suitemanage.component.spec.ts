import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SuitemanageComponent } from './suitemanage.component';

describe('SuitemanageComponent', () => {
  let component: SuitemanageComponent;
  let fixture: ComponentFixture<SuitemanageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SuitemanageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SuitemanageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
