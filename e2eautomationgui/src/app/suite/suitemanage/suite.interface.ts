export interface Suite {
    suiteId:number;
    name: string; // required field with minimum 5 characters
    env: string; // required field with minimum 5 characters
    testcases: TestCase[]; // user can have one or more addresses
}

export interface TestCase {
    testcaseId: string;  // required field
    testcaseName: string;
    testenv: string;
}
