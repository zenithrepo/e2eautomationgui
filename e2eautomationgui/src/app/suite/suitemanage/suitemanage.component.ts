import { Component, OnInit,NgModule } from '@angular/core';
import { FormGroup, FormArray, FormBuilder, Validators } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClient, HttpHeaders  } from '@angular/common/http';
import { Observable } from 'rxjs/Rx';
import { Suite } from './suite.interface';
import {IOption} from 'ng-select';
import { ServerResponse } from "../../server.response";
import { NgProgress } from 'ngx-progressbar';
import { Router } from '@angular/router';

import { Http, Response, Headers, RequestOptions } from '@angular/http';

const httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Component({
  selector: 'app-suitemanage',
  templateUrl: './suitemanage.component.html',
  styleUrls: ['./suitemanage.component.css']
})
export class SuitemanageComponent implements OnInit {
  public suiteForm: FormGroup;
  public submitted: boolean; // keep track on whether form is submitted
  private baseUrl: string =  'http://localhost:9200/E2EAutomation/GUIController?param=';
  testcaseOptions: Array<IOption>;
  private header = new Headers({'Content-Type':'application/json'});
  private options = new RequestOptions({headers:this.header});
  private response: ServerResponse[];

  
  constructor(private _fb: FormBuilder ,private router: Router, private http:HttpClient ,private normalhttp:Http,public ngProgress: NgProgress ) { }

  getTestCases()
  {
   console.log('getting suite data');
   return this.normalhttp.get(this.baseUrl + 'tselect', this.options).map((response: Response) => response.json()).catch(this.errorHandler);
     // (response:Response) => response.json()).catch(this.errorHandler)
  }
  
     errorHandler(error:Response)
  {
    return Observable.throw(error||"Server connection error");
  }
  
    cancelForm()
  {
    this.router.navigate(['/suite']);
  }
  
    ngOnInit() {
      this.getTestCases().subscribe((TestCasesList) => {
      this.testcaseOptions = TestCasesList;
      console.log(this.testcaseOptions);
        }),(error) => {
         console.log("error")
         }
      
      this.suiteForm = this._fb.group({
        name: ['', [<any>Validators.required]],
        env: ['', [<any>Validators.required]],
        testcases: this._fb.array([this.initTestCases()])
      });
  
    /*  (<FormGroup>this.suiteForm)
            .setValue(people, { onlySelf: true });
      */
      
    }

  /*
    This creates a new formgroup. You can think of it as adding an empty object
    into an array. So we are pushing an object to the formarray 'itemrows' that
    has the property 'itemname'. 
    */
    initTestCases() {
        return this._fb.group({
            testcaseName: ['',Validators.required],
            testenv: [],
        });
    }

  save(suite:Suite) {
   if (confirm('Are you sure to submit "'+suite.name+'" suite ?') == true)
  {
     this.submitted = true;
      console.log("Form Submitted!");
      console.log(suite.env);
	  this.createSuite(suite);
      //this.suiteForm.reset();
  }
  }
    addTestCase() {
        const control = <FormArray>this.suiteForm.controls['testcases'];
        control.push(this.initTestCases());
    }

    deleteTestCase(index: number) {
        const control = <FormArray>this.suiteForm.controls['testcases'];
        control.removeAt(index);
    }
    createSuiteHttp(suite) {
        let body = JSON.stringify(suite);
        console.log("sending http post request for create");
        return this.http.post(this.baseUrl+'createsuite', body, httpOptions);
      //return this.http.post(this.baseUrl+'createsuite', body, httpOptions).map((response: Response) => response.json()).catch(this.errorHandler);      
    }
    updateSuiteHttp(suite) {
        let body = JSON.stringify(suite);
        return this.http.put(this.baseUrl+'updatesuite' + suite.suiteId, body, httpOptions);
    }

    deleteSuiteHttp(suite) {
        return this.http.delete(this.baseUrl+'deletesuite'  + suite.suiteId);
    }

  createSuite(suite) {
    this.ngProgress.start();
    this.createSuiteHttp(suite).subscribe(
       (data) => {
         //this.response = data;
         console.log("printing response data");
         console.log(data[0].errorMessage);
         console.log(data[0].status);
         this.ngProgress.done();
         alert(data[0].errorMessage);
         this.router.navigate(['/suite']);         
       }
      /*,
       error => {
         console.error("Error insaving Suite!");
         return Observable.throw(error);
       } */
    ),(error) => {console.log("error");}
    console.log('Run completed');
  }

  updateSuite(suite) {
    this.updateSuiteHttp(suite).subscribe(
       data => {
          // forward to suite list
         return true;
       },
       error => {
         console.error("Error update Suite!");
         return Observable.throw(error);
       }
    );
  }

  deleteSuite(suite) {
    if (confirm("Are you sure you want to delete " + suite.name+  "?")) {
      this.deleteSuiteHttp(suite).subscribe(
         data => {
            // forward to suite list
           return true;
         },
         error => {
           console.error("Error deleting Suite!");
           return Observable.throw(error);
         }
      );
    }
  }
  }