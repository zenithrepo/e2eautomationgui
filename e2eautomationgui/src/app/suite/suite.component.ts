import { ServerResponse } from "../server.response";
import { Component, OnInit } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
import {Testsuite} from '../testsuite/testsuite';

import { NgProgress } from 'ngx-progressbar';

import { throws } from 'assert';

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Component({
  selector: 'app-suite',
  templateUrl: './suite.component.html',
  styleUrls: ['./suite.component.css']
})
export class SuiteComponent implements OnInit {

    headerRow: string[];

    dtOptions: DataTables.Settings = {};
    private successAlert:boolean;
    private selectedSuiteId: number;
    private response: ServerResponse[];
    private TestSuiteArray: Testsuite[]; 
    private baseUrl: string = 'http://localhost:9200/E2EAutomation/GUIController?param=';
    private header = new Headers({'Content-Type':'application/json'});
    private options = new RequestOptions({headers:this.header});
    private start : true;
    private running : boolean = false;

    dtTrigger: Subject<any> = new Subject();
    constructor( private http: Http, private router: Router,public ngProgress: NgProgress ){}

  getTestSuites()
  {
   console.log('getting suite data');
   return this.http.get(this.baseUrl + 'suite', this.options).map((response: Response) => response.json()).catch(this.errorHandler);
     // (response:Response) => response.json()).catch(this.errorHandler)
  }
  errorHandler(error:Response)
  {
    return Observable.throw(error||"Server connection error");
  }
  removeAlert()
  {
     this.successAlert = false;
     this.response[0] = null;
  }
   success(suiteID: string) {
     this.router.navigate(['/suitedetails/',suiteID, "run","0"]);
    }
   connectToRunTS(testsuiteID){
    return this.http.get(this.baseUrl + 'run&sid=' + testsuiteID, this.options).map((response: Response) =>
       response.json()).catch(this.errorHandler);
  } 
  run(TestSuite)
  {
    console.log('Inside the Run method');
    
    this.ngProgress.start();
    this.running = true;
    this.connectToRunTS(TestSuite.suiteId).subscribe((resp) => {
    this.response = resp;
    console.log(this.response[0]);
      this.ngProgress.done();
      this.running = false;
    this.success(TestSuite.suiteId );
      
  }),(error) => {console.log("error");}
    console.log('Run completed');
  }

  deletTestSuiteHttp(sid:number) {
   
        return this.http.get(this.baseUrl+'deletesuite&sid='+sid).map((response: Response) => response.json()).catch(this.errorHandler);
    }

  removeTS(testsuite:Testsuite)
  {
    if (confirm('Are you sure to delete "'+testsuite.name+'" suite ?') == true)
     {
      this.deletTestSuiteHttp(testsuite.suiteId).subscribe(
       (data) => {
         //this.response = data;
         console.log("printing response data");
         console.log(data[0].errorMessage);
         console.log(data[0].status);
         alert(data[0].errorMessage);
          this.refreshTestSData();
       }
    ),(error) => {console.log("error");}
    console.log('delete completed');
     }
  }
   refreshTestSData()
{
this.getTestSuites().subscribe((TestSuites) => {
    this.TestSuiteArray = TestSuites;    
  }),(error) => {
    console.log("error")
  }
}
    ngOnInit() {
      this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 25
    }; 
    

      
      this.getTestSuites().subscribe((TestSuites) => {
      this.TestSuiteArray = TestSuites;
      console.log(this.TestSuiteArray[0].executionCounter);        
        this.dtTrigger.next();        
        }),(error) => {
         console.log("error")
         }
      
      
      this.headerRow = ['#', 'Test Suite', 'Env', 'Test Cases', 'Creation Date'];
  }

}
