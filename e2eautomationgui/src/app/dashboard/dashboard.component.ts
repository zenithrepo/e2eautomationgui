import { Dashboardsummary } from './dashboardsummary';
import { Component, OnInit, Injectable } from '@angular/core';
import { Http, Response , Headers , RequestOptions} from '@angular/http';

import * as Chartist from 'chartist';
import { Subscriber, Observable } from 'rxjs';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

declare var $:any;

declare interface TableData {
    headerRow: string[];
    dataRows: string[][];
}
@Component({
    selector: 'dashboard-cmp',
    moduleId: module.id,
    templateUrl: 'dashboard.component.html'
})

export class DashboardComponent implements OnInit {
  public pendingtestsuite: TableData;
  totalTestSuites: string = '0';
  completedTestcase: string = '0';
 
  private dashboardSummaryArray: Dashboardsummary[]; 
  private dashboardSummary: Dashboardsummary;  
  private baseUrl: string = 'http://localhost:9200/E2EAutomation/GUIController?param=';   
  private header = new Headers({'Content-Type':'application/json'});

  private options = new RequestOptions({headers: this.header});
   
  constructor( private http: Http ){}
  
  fetchDashboardSummary()
  {
    console.log('inside the fetchDashboardSummary');
    return this.http.get(this.baseUrl+'dashboardSummary', this.options).map((response:Response) => response.json()).catch(this.errorHandler)
  // return this.http.get(this.baseUrl+'suite', this.options).map((response:Response) => response.json()).catch(this.errorHandler)   
     // (response:Response) => response.json()).catch(this.errorHandler)
  }
  
  errorHandler(error:Response)
  {
    return Observable.throw(error || "Server connection error");
  }
   circlechart()
  {
   this.http.get(this.baseUrl+'suite').map(
      (response) => response.json()).subscribe((data) => console.log(data)); 
  }
  
  
    ngOnInit(){
   
      this.fetchDashboardSummary().subscribe((dashboardSummaryResp) => {
        console.log('response from fetchDashboardSummary');
        console.log(dashboardSummaryResp)
        this.dashboardSummaryArray = dashboardSummaryResp;
        this.dashboardSummary = this.dashboardSummaryArray[0]
        console.log(this.dashboardSummary.totalSuites)
      }),(error) => {
        console.log("error")
      }
      
        var dataSales = {
          labels: ['9:00AM', '11:00AM', '1:00PM', '3:00PM', '5:00PM', '7:00PM','9:00PM'],
          series: [
            [0, 0, 12, 20, 30, 40,80],
            [0, 0, 1, 2, 3, 5,20],
            [0, 0, 6, 8, 0, 4,10]
          ]
        };
        
       
        var optionsSales = {
          low: 0,
          high: 100,
          showArea: true,
          height: "245px",
          axisX: {
            showGrid: false,
          },
          lineSmooth: Chartist.Interpolation.simple({
            divisor: 3
          }),
          showLine: true,
          showPoint: true,
        };

        var responsiveSales: any[] = [
          ['screen and (max-width: 640px)', {
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];

      this.pendingtestsuite = {
            headerRow: [ 'TS ID', 'TS Name', 'Release', 'Env', 'Created by', 'Excution Status','Executed by'],
            dataRows: [
              ['1', 'SAPCC WCC AT', '2018 Apr - Wireless Release', 'PT140', 'Sagar','Pending',''],
              ['2', 'SDF WCC AT', '2018 Apr - Wireless Release', 'PT140', 'Sagar','Pending',''],
              ['3', 'SAPCC WCC PT', '2018 Apr - Wireless Release', 'ST', 'Sagar','Pending',''],
              ['4', 'SAPCC WCC PROD', '2018 Apr - Wireless Release', 'PROD', 'Sagar','Pending',''],
              ['5', 'KB WRP-II', '2018 July - Wireless Release', 'PT140', 'Sagar','Pending',''],
                
            ]
        };
      
        new Chartist.Line('#chartHours', dataSales, optionsSales, responsiveSales);


        var data = {
          labels: ['TS1', 'TS2', 'TS3', 'TS4', 'TS5', 'TS6', 'TS7', 'TS8'],
          series: [
            [10, 20, 18, 34, 54, 75, 100, 43],
            [2, 0, 3, 3, 3, 4, 20, 4],
            [4, 0, 0, 1, 5, 0, 0, 6]
          ]
        };

        var options = {
            seriesBarDistance: 10,
            axisX: {
                showGrid: false
            },
            height: "245px"
        };

        var responsiveOptions: any[] = [
          ['screen and (max-width: 640px)', {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              }
            }
          }]
        ];

        new Chartist.Line('#chartActivity', data, options, responsiveOptions);

        var dataPreferences = {
            series: [
                [25, 30, 20, 25]
            ]
        };

        var optionsPreferences = {
            donut: true,
            donutWidth: 40,
            startAngle: 0,
            total: 100,
            showLabel: false,
            axisX: {
                showGrid: false
            }
        };

      
      
        new Chartist.Pie('#chartPreferences', dataPreferences, optionsPreferences);

        new Chartist.Pie('#chartPreferences', {
          labels: ['62%','32%','6%'],
          series: [62, 32, 6]
        });
    }
}
