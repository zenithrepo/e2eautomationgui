export class Dashboardsummary {
   totalSuites:string;
   totalTestcases:string;
   successSuites:string;
   failedTestCases:string;
   successTestCases:string;
   skippedTestCases:string;
   successSteps:string;
   failedSteps:string;
   date:string;
}