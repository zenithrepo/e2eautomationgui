import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { AppRoutes } from './app.routing';
import { SidebarModule } from './sidebar/sidebar.module';
import { FooterModule } from './shared/footer/footer.module';
import { NavbarModule} from './shared/navbar/navbar.module';
import { FormsModule , ReactiveFormsModule } from '@angular/forms';
import { JsonSchemaFormModule } from 'angular4-json-schema-form-updated';
import { DataTablesModule } from 'angular-datatables';
import { NgProgressModule } from 'ngx-progressbar';
import { AceEditorModule } from 'ng2-ace-editor';

import { DashboardComponent } from './dashboard/dashboard.component';
import { UserComponent } from './user/user.component';
import { TableComponent } from './testsuite/table.component';
import { TypographyComponent } from './typography/typography.component';
import { IconsComponent } from './icons/icons.component';
import { NotificationsComponent } from './notifications/notifications.component';
import { SuitedetailsComponent } from './suite/suitedetails/suitedetails.component';
import { SuiteComponent } from './suite/suite.component';
import { HistoryComponent } from './history/history.component';
import { SuitemanageComponent } from './suite/suitemanage/suitemanage.component';
import { SelectModule } from 'ng-select';
import { TestcasemanageComponent } from './testsuite/testcasemanage/testcasemanage.component';
import { SettingsComponent } from './settings/settings.component';


@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    UserComponent,
    TableComponent,
    TypographyComponent,
    IconsComponent,
    NotificationsComponent,
    SuitedetailsComponent,
    SuiteComponent,
    HistoryComponent,
    SuitemanageComponent,
    TestcasemanageComponent,
    SettingsComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(AppRoutes),
    SidebarModule,
    HttpModule,
    NavbarModule,
    FooterModule,
    FormsModule,
    JsonSchemaFormModule,
    DataTablesModule,
    NgProgressModule,
    ReactiveFormsModule,
    HttpClientModule,
    SelectModule,
    AceEditorModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
