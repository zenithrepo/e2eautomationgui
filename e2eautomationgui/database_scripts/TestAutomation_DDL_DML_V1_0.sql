
CREATE TABLE TB_TESTCASES(
  testcase_id     bigint PRIMARY KEY AUTO_INCREMENT,  
  testcase_name   varchar(100) NOT NULL,
  testcase_xml    mediumtext,
  env             varchar(20),
  tag             varchar(400),
  hpqc_id         varchar(20),
  description     text,
  state           varchar(20) DEFAULT 'ACTIVE',
  created_by      varchar(20),
  updated_by      varchar(20),
  creation_date   timestamp DEFAULT CURRENT_TIMESTAMP,
  updated_date    timestamp
)ENGINE=InnoDB;

CREATE TABLE TB_SUITES(
  suite_id        bigint PRIMARY KEY AUTO_INCREMENT,  
  suite_name      varchar(100) NOT NULL,
  env             varchar(20),
  created_by      varchar(20),
  updated_by      varchar(20),
  creation_date   timestamp DEFAULT CURRENT_TIMESTAMP,
  updated_date   timestamp
)ENGINE=InnoDB;

CREATE TABLE TB_SUITES_TESTCASES(
  suite_id        bigint,
  testcase_id     bigint,
  execution_sq    int,
  PRIMARY KEY(suite_id,testcase_id),
  FOREIGN KEY (suite_id) REFERENCES TB_SUITES(suite_id) ON DELETE CASCADE,
  FOREIGN KEY (testcase_id) REFERENCES TB_TESTCASES(testcase_id)
);


CREATE TABLE TB_REPORT(
  test_instance   bigint,
  suite_id        bigint,
  testcase_id     bigint default 0,   
  test_level      int default 3,
  step_number     int default 0,
  name            varchar(100),
  status          varchar(10),
  message         text,
  env             varchar(20),
  executed_by     varchar(20),
  start_ts        timestamp,
  end_ts          timestamp,
  PRIMARY KEY  (test_instance,suite_id,testcase_id,test_level,step_number)
);

CREATE INDEX TB_REPORT_INDEX ON TB_REPORT (start_ts,end_ts);


insert into TB_TESTCASES (testcase_name,testcase_xml,env,tag,description,created_by)values ('TC1_AllowanceExhaust','<test-case test-case-id="1" name="TC1_AllowanceExhaust" env="PT140"><test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" />   	<test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>        <test-assert class="com.zenith.automation.framework.TdrImpl" session-id="POC_DATA_201803160500" assert-type="CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100"/>        <test-assert class="com.zenith.automation.framework.SapccDbImpl" sub="S8414167" assert-type="METER01=100" wait-timer-sec="5" /></test-case>','PT140','sample case','description is information that has been translated into a form that is efficient for movement or processing. Relative to todays computers and transmission media, data is information converted into binary digital form. It is acceptable for data to be used as a singular subject or a plural subject. Raw data is a term used to describe data in its most basic digital format.' , 'dilipp' );

insert into TB_TESTCASES (testcase_name,testcase_xml,env,tag,description,created_by)values ('TC1_AllowanceExhaust1','<test-case test-case-id="2" name="TC1_AllowanceExhaust1" env="PT148"><test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" />   	<test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>        <test-assert class="com.zenith.automation.framework.TdrImpl" session-id="POC_DATA_201803160500" assert-type="CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100"/>        <test-assert class="com.zenith.automation.framework.SapccDbImpl" sub="S8414167" assert-type="METER01=100" wait-timer-sec="5" /></test-case>','PT148','sample case','description is information that has been translated into a form that is efficient for movement or processing. Relative to todays computers and transmission media, data is information converted into binary digital form. It is acceptable for data to be used as a singular subject or a plural subject. Raw data is a term used to describe data in its most basic digital format.' , 'dilipp' );

insert into TB_TESTCASES (testcase_name,testcase_xml,env,tag,description,created_by)values ('TC1_AllowanceExhaust2','<test-case test-case-id="3" name="TC1_AllowanceExhaust2" env="PT168"><test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" />   	<test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>        <test-assert class="com.zenith.automation.framework.TdrImpl" session-id="POC_DATA_201803160500" assert-type="CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100"/>        <test-assert class="com.zenith.automation.framework.SapccDbImpl" sub="S8414167" assert-type="METER01=100" wait-timer-sec="5" /></test-case>','PT168','sample case','description is information that has been translated into a form that is efficient for movement or processing. Relative to todays computers and transmission media, data is information converted into binary digital form. It is acceptable for data to be used as a singular subject or a plural subject. Raw data is a term used to describe data in its most basic digital format.' , 'dilipp' );


insert into TB_TESTCASES (testcase_name,testcase_xml,env,tag,description,created_by)values ('TC1_AllowanceExhaust3','<test-case test-case-id="4" name="TC1_AllowanceExhaust3" env="ST"><test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" />   	<test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>        <test-assert class="com.zenith.automation.framework.TdrImpl" session-id="POC_DATA_201803160500" assert-type="CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100"/>        <test-assert class="com.zenith.automation.framework.SapccDbImpl" sub="S8414167" assert-type="METER01=100" wait-timer-sec="5" /></test-case>','ST','sample case','description is information that has been translated into a form that is efficient for movement or processing. Relative to todays computers and transmission media, data is information converted into binary digital form. It is acceptable for data to be used as a singular subject or a plural subject. Raw data is a term used to describe data in its most basic digital format.' , 'dilipp' );

insert into TB_TESTCASES (testcase_name,testcase_xml,env,tag,description,created_by)values ('TC1_AllowanceExhaust4','<test-case test-case-id="4" name="TC1_AllowanceExhaust3" env="ST"><test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" /> <test-prerequisites class="com.zenith.automation.framework.SapccDbImpl" type="TABLE" sub="S8414167" precheck-fields="METER*" />  	<test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>    <test-step class="com.zenith.automation.framework.OnlineMZImpl" session-id="POC_DATA_201803160500" ban="70814662" sub="994373488080" rating-group="" mcc-mnc="" usage-type="domesti" usage="2248"/>     <test-assert class="com.zenith.automation.framework.TdrImpl" session-id="POC_DATA_201803160500" assert-type="CHARGING-CHARACTERISTICS=40,UNITS-CONSUMED=125,CHARGE-AMOUNT=1.22,SERVICE-IDENTIFIER=DATA,BILL-MONTH-NUM=35,UNITS-CONSUMED=100,GRANTED-SERVICE-UNITS=100"/>        <test-assert class="com.zenith.automation.framework.SapccDbImpl" sub="S8414167" assert-type="METER01=100" wait-timer-sec="5" /></test-case>','TS7','WCC case','description is information that has been translated into a form that is efficient for movement or processing. Relative to todays computers and transmission media, data is information converted into binary digital form. It is acceptable for data to be used as a singular subject or a plural subject. Raw data is a term used to describe data in its most basic digital format.' , 'dilipp' );

insert into TB_SUITES ( suite_id  , suite_name   , env     ) values(1,'SAPCC suite','PT148');
insert into TB_SUITES ( suite_id  , suite_name   , env     ) values(2,'SAPCC suite2','PT140');
insert into TB_SUITES ( suite_id  , suite_name   , env     ) values(3,'SAPCC suite3','PT168');
insert into TB_SUITES ( suite_id  , suite_name   , env     ) values(4,'SAPCC suite4','ST');
insert into TB_SUITES ( suite_id  , suite_name   , env     ) values(5,'SAPCC suite5','ST');

insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (1,5,1);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (1,6,2);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (1,7,3);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (2,7,1);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (2,8,2);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (3,8,1);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (4,8,1);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (5,8,1);
insert into TB_SUITES_TESTCASES( suite_id , testcase_id ,  execution_sq) values (5,7,2);



select s.suite_id as suite_id , s.suite_name as suite_name, s.env as senv, t.testcase_name as testcase_name from join ,   as t where s.suite_id = st.suite_id;


select s.suite_id as suite_id , s.suite_name as suite_name, s.env as senv, ( select  testcase_name  from   TB_TESTCASES t  where t.testcase_id = st.testcase_id order by st.execution_sq) as testcase_name from TB_SUITES as s join TB_SUITES_TESTCASES as st on st.suite_id=s.suite_id;
			
			
select s.suite_id 


select status, COUNT(status) as sum ,((count(status)/(SELECT COUNT(*) from tb_report as t where t.test_instance =?)) * 100) as percentage  from tb_report as r where r.status IS NOT NULL and r.test_instance=? group by r.status
					
					
					
select DISTINCT  (test_instance) , testcase_id, suite_id , complete_date , status from tb_report where suite_id= ? order by test_instance DESC


select test_instance , suite_id   ,  testcase_id  ,  test_level  ,  step_name ,step_number , status   ,  result_message   ,  env      ,   executed_by   ,  start_date   ,  complete_date   from  TB_REPORT  where test_instance = 1 and suite_id = 1;



insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,11,18,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,11,18,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,11,18,3,3,'Tdr','success','Tdr step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,11,18,3,4,'SapccDb','success','SapccDb assert step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (1,11,18,2,'TC1_AllowanceExhaust','success','TC1_AllowanceExhaust step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,12,18,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (1,12,18,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (1,12,18,2,'TC2_AllowanceExhaust','success','TC2_AllowanceExhaust testcase success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (1,0,18,1,'E2E_Test','success','E2E_Test suite','PT140','system',now(),now());

insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,11,18,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,11,18,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,11,18,3,3,'Tdr','success','Tdr step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,11,18,3,4,'SapccDb','success','SapccDb assert step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,11,18,2,'TC1_AllowanceExhaust','success','TC1_AllowanceExhaust step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,12,18,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,12,18,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,12,18,2,'TC2_AllowanceExhaust','success','TC2_AllowanceExhaust testcase success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,0,18,1,'E2E_Test','success','E2E_Test suite','PT140','system',now(),now());


insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,1,1,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,1,1,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,1,1,3,3,'Tdr','success','Tdr step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,1,1,3,4,'SapccDb','success','SapccDb assert step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,1,1,2,'TC1_AllowanceExhaust','success','TC1_AllowanceExhaust step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,2,1,3,1,'Sapcc','success','SAPCC step success','TS27','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,step_number,name,status,message,env,executed_by,start_ts,end_ts)values (2,2,1,3,2,'OnlineMZ','success','OnlineMZ step success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,2,1,2,'TC2_AllowanceExhaust','success','TC2_AllowanceExhaust testcase success','PT140','system',now(),now());
insert into TB_REPORT(test_instance,testcase_id,suite_id,test_level,name,status,message,env,executed_by,start_ts,end_ts)values (2,0,1,1,'E2E_Test','success','E2E_Test suite','PT140','system',now(),now());

