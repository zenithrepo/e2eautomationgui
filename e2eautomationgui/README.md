# [TestAutomation]

Version:- 1.0.0
Date :- 27-Apr-2018

Step1 :- Install Node JS
Download and Install the node-v8.11.1-x64.msi file from nodejs website

Step 2 :- install the packages

Go inside the application from cmd

C:\Users\x172875\Documents\Eclipse-Zenith-WorkSpace\e2eautomationgui>

Check version of installed software 

C:\Users\x172875\Documents\Eclipse-Zenith-WorkSpace\e2eautomationgui> ng version

Install the packages

C:\Users\x172875\Documents\Eclipse-Zenith-WorkSpace\e2eautomationgui>npm install

Start the application

C:\Users\x172875\Documents\Eclipse-Zenith-WorkSpace\e2eautomationgui>ng serve

Test from browser 

http://localhost:4200/


Step 3:- start from history module (this is completely new module)

Versions used for this project :- 
Angular CLI: 1.7.4
Node: 8.11.1
NPM verson 5.6.0
Angular 5


## Links:

+ [Live Preview]()


## Terminal Commands

1. Install NodeJs from [NodeJs Official Page](https://nodejs.org/en).
2. Open Terminal
3. Go to your file project
4. Run in terminal: ```npm install -g @angular/cli```
5. Then: ```npm install```
6. And: ```ng serve```
7. Navigate to: [http://localhost:4200/](http://localhost:4200/)

### What's included

## Useful Links

